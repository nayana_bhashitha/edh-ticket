"use strict";

var app = angular.module('edhirya-it');
app.controller('Preloader',function($scope,$rootScope){

    $scope.$on('signinBegin', function() {
        $scope.loader = true;
    });

    $scope.$on('signinError', function() {
        $scope.loader = false;
    });
});